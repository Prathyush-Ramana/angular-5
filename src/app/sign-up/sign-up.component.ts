import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Country } from '../country';
import { DataServiceService } from '../data-service.service';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {


  constructor(private dataService: DataServiceService) {
  }
  private _registrationURL = "http://localhost/RegisterUser/api/RegisterUser";
  valuesubmited: string = "";
  errorMessage: string;

  ngOnInit() {
  }
  selectedCountry: Country = new Country(2, 'India');
  countries = [
    new Country(1, 'USA'),
    new Country(2, 'India'),
    new Country(3, 'Australia'),
    new Country(4, 'Brazil')
  ];

  // onSelect(countryId) {
  //   debugger
  //   this.selectedCountry = null;
  //   for (var i = 0; i < this.countries.length; i++) {
  //     if (this.countries[i].id == countryId) {
  //       this.selectedCountry = this.countries[i];
  //     }
  //   }
  // }

  registerUser(form: NgForm) {
    this.valuesubmited = form.value;
    console.log(this.valuesubmited);
    this.dataService.createService(this._registrationURL, this.valuesubmited)
      .subscribe(result => console.log(result),
      error => this.errorMessage = <any>error);
    //form.reset();
  }
}
