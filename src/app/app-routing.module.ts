import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationFormComponent} from './registration-form/registration-form.component'
import { SignUpComponent } from './sign-up/sign-up.component'

const routes: Routes = [
  {path: "Registration", component: RegistrationFormComponent},
  {path: "SignUp", component: SignUpComponent},
  {path: "", redirectTo: "/SignUp", pathMatch: "full"},
  {path: "**", component: SignUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
