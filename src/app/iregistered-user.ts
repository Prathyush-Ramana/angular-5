export interface IregisteredUser {
    firstName : string;
    lastName : string;
    country: string;
    emailId : string;
}
